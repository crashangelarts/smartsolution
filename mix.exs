defmodule Smart.MixProject do
  use Mix.Project

  def project do
    [
      app: :smart,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      applications: applications(Mix.env),
    ]
  end

  defp applications(:dev), do: applications(:all) ++ [:remix]
  defp applications(_all), do: [:logger, :httpoison]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:earmark, "~> 1.2", only: :dev},
      {:ex_doc, "~> 0.19", only: :dev},
      
      # Hot Reload
      {:remix, "~> 0.0.1", only: :dev},

      #HttpRequest
      {:httpoison, "~> 1.6"},

      #Html
      {:html_entities, "~> 0.4"}
    ]
  end
end