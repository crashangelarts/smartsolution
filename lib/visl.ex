defmodule Visl do
  @moduledoc """
  -----------------------------------
  Visual Interactive Syntax Learning
  University of Southern Denmark
  -----------------------------------
  Este módulo tem como responsabilidade 
  tratar todas as atividades que necessitam de conversão 
  do idioma Português utilizando Processamento de Linguagem Natural em 

    - Análises Sintática (Dependências das Palavras)
    - Atributos de Palavras (Verbos, Substantivos, Adjetivos, Pronomes, etc)
  """

  @doc """
  Envia a entrada de texto enviada pelo usuário para que o Visl faça análise
  sintática de reconhecimento de texto

  ## Examples

      iex> Visl.call("Ola")

  """
  def call(input) do
    query = HtmlEntities.encode(String.replace(input, " ", "%20"))

    case HTTPoison.get(
           'https://visl.sdu.dk/cgi-bin/visl.pt.cgi?parser=roles&visual=cg-dep&symbol=cg&inputlang=pt&text=#{
             query
           }',
           [],
           ssl: [versions: [:"tlsv1.2"]]
         ) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} -> Visl.creste_pattern(body)
      {:ok, %HTTPoison.Response{status_code: 404}} -> IO.puts("Not found :(")
      {:error, %HTTPoison.Error{reason: reason}} -> IO.inspect(reason)
    end
  end

  def dev do
    # Visl.call("Um paragrafo inteiro, com complementos. Uma nova sentença com acentos!")
    test =
      "<ß>\r\n\r\nUm  [um]  <*> <arti> DET M S  @>N #1->2 \r\nparagrafo  [paragrafo]  <DERP> <tool> N M S  @NPHR §DENOM #2->0 \r\ninteiro  [inteiro]  <np-close> ADJ M S  @N< #3->2 \r\n,  [,]  PU @PU #4->0 \r\ncom  [com]  <np-close> PRP  @N<PRED §ATR #5->2 \r\ncomplementos  [complemento]  <act> N M P  @P< #6->5 \r\n.  [.]  PU @PU #7->0 \r\n\r\n\r\n\r\n\r\n<ß>\r\n\r\n\r\nUma  [um]  <*> <arti> DET F S  @>N #1->3 \r\nnova  [novo]  <jh> ADJ F S  @>N #2->3 \r\nsentença  [sentença]  <sem-s> N F S  @NPHR §DENOM #3->0 \r\ncom  [com]  <np-close> PRP  @N< #4->3 \r\nacentos  [acento]  <ac-sign> N M P  @P< §COM-ADV #5->4 \r\n!  [!]  PU @PU #6->0 \r\n\r\n\r\n\r\n\r\n\r\n"

    Visl.creste_pattern(test)
  end

  def creste_pattern(r) do
    r = HtmlEntities.decode(r)
    r = String.replace(r, "<dl><br><br>", "")
    r = String.replace(r, "<dt><b>", "")
    r = String.replace(r, "<font color=\"maroon\">", "")
    r = String.replace(r, "</font>", "")
    r = String.replace(r, "<font color=\"darkgreen\">", "")
    r = String.replace(r, "</b>", "")
    r = String.replace(r, "<font color=\"blue\">", "")
    r = String.replace(r, "<b>", "")
    r = String.replace(r, "	", "")
    r = String.replace(r, "<br>", "\r\n")
    r = String.replace(r, "</dl>", "")
    r = String.replace(r, "</ß>", "")
    IO.puts r
    itens = String.split(r, "\r\n", trim: true)

    Enum.each(itens, fn i ->
      if i == "<ß>" do
        IO.puts("olha eu aqui")
      end

      # IO.inspect i
    end)

    # IO.puts r
  end
end
